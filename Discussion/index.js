// CRUD Operations

/*
	- CRUD operations are the heart of any backend application
	- CRUD stands for Create, Read, Update, and Delete
	- MongoDB deals with objects as it's structure for documents, we can easily create then by providing objects into out methods.
*/

// Create : Inserting documents

// Insert One
/*
	Syntax:
		db.collectionName.insertOne({})

	Inserting/Assigning values in JS Objects:
		object.object.method({object})
*/

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "5875698",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
});

// Insert Array
/*
	Syntax:
		db.users.inserMany([
		{objectA},
		{objectB}
	])
*/

db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "5878787",
			email: "stephenhawking@gmail.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "5552252",
			email: "neilarmstrong@gmail.com"
		},
		courses: ["React", "Laravel", "SASS"],
		department: "none"
	}		
])


// Read : Finding documents

// Finding Alll
/*
	Syntax:
		db.collectionName.find();
*/

db.users.find();

// Finding users with single arguments
/*
	Syntax:
		db.collectionName.find({field: value})
*/

// Look for Stephen Hawking using firstaname
db.users.find({firstName: "Stephen"})

// Finding users with multiple arguments
db.users.find({firstName: "Stephen", age: 20}); //0 records - no match

db.users.find({firstName: "Stephen", age: 76}); //with match match

// Update: Updating documents
// Repeat Jane to be updated

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "5875698",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
});

// Uodat One
/*
	Syntax:
		db.collectionName.updateOne({criteria}, {$set: {field: value}})
*/

db.users.updateOne({firstName: "Jane"},
	{
	$set: {
		firstName: "Jane",
		lastName: "Gates",
		age: 65,
		contact: {
			phone: "5875698",
			email: "janedoe@gmail.com"
		},
		courses: ["AWS", "Google Cloud", "Azure"],
		department: "infrastructure",
		status: "active"
	}
});

db.users.find({firstName: "Jane"});

// Update Many
db.users.updateMany(
	{department: "none"},
	{
		$set: {
			department: "HR"
		}
	}
)

db.users.find();

// Replace one
/*
	- Can be used if replacing the whole document is necessary
	Syntax:
		db.collectionName.replaceOne({criteria}, {$field: value})
*/

db.users.replaceOne(
	{lastName: "Gates" },
    {
		firstName: "Bill",
		lastName: "Clinton"
	}
)
db.users.find({firstName: "Bill"});


// Delete: Deleting documents
db.users.insert({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "0000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
})
db.users.find({firstName: "Test"})

// Deleting a single document
/*
	Syntax:
		db.collectionName.deleteOne({criteria})
*/

db.users.deleteOne({firstName: "Test"});
db.users.find({firstName: "Test"});

// Delete many
db.users.deleteMany({courses: []});
db.users.find();

// Delete all
// db.users.delete()
/*
	- DO NOT USE: db.collectionName.deleteMany()
	Syntax:
		db.collectionName.deleteMany({criteria})
*/

// Advance Queries

// Query an embedded document
db.users.find({
	contact : {
        phone : "5875698",
        email : "janedoe@gmail.com"
    }
})

// Querying on nested fields
db.users.find({"contact.email":"janedoe@gmail.com"})

// Querying an array with exact elements
db.users.find({courses: ["React", "Laravel", "SASS"]})

// Quering an element without regard to order
db.users.find({courses: {$all: ["SASS", "React", "Laravel"]}})

// Make an array to query
db.users.insert({
	namearr: [
		{
			namea: "juan"
		},
		{
			nameb: "tamad"
		}
	]
})

// find
db.users.find({
	namearr: {
		namea: "juan"
	}
})